<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('DB_NAME', 'db_0aa13ae3');
define('DB_NAME', $_SERVER["DB_NAME"]);


/** MySQL database username */
//define('DB_USER', 'user_0aa13ae3');
define('DB_USER', $_SERVER["DB_USER"]);

/** MySQL database password */
//define('DB_PASSWORD', 'SV^le8eMRik6P,');
define('DB_PASSWORD', $_SERVER["DB_PASS"]);

/** MySQL hostname */
define('DB_HOST', $_SERVER["DB_HOST"]);
//define('DB_HOST', 'a.db.shared.orchestra.io');
//define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',        'vM5ZUUf!`)jo+H`j0E-./Jh45,$:Y|3cy9521`NMT9;q@L3Y1&c?$eH`kC0X6-Vs');
define('SECURE_AUTH_KEY', 'g3WTc::G)w+I9E9gP.4W+e_Zj@E!(>,QuPZeM21!F1};@3|K?5%vCXj49Qf+Xe`.');
define('LOGGED_IN_KEY',   'm`W]THfs`npgXc ){GR-#eV#^#8#-O*<ck0*hK}| K4hGIfF)1T*~X#,TF|1sB,A');
define('NONCE_KEY',       'sW5iTFnEC`,vW-h(F[#&}wvT->43IW!a#E^Aw(8n@Kf|hC#[{NXI/GdF$&qteTH;');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', 'pt_BR');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
