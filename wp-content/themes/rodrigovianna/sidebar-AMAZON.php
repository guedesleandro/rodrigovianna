﻿<div id="sidebar">
	<div id="videos">
		<? $tv = get_highlight('video') ?>
		<? foreach($tv as $current): ?>       
		<h2><? the_title() ?></h2>
		<a href="<?= get_category_link(get_category_by_slug('videos')) ?>"><span class="more">Mais vídeos »</span></a>
	  <div class="div_medium clear"></div>
		<object width="340" height="260">
			<param name="movie" value="http://www.youtube.com/v/<?= $current['meta']['youtube']['0'] ?>&amp;rel=0">
			<param name="wmode" value="transparent">
			<embed src="http://www.youtube.com/v/<?= $current['meta']['youtube']['0'] ?>&amp;rel=0" type="application/x-shockwave-flash" wmode="transparent" width="336" height="203">
		</object>
		<p><? the_excerpt() ?></p>
		<? endforeach; ?>  
	</div>
	<!-- #videos -->
    
    <div style="text-align: center; margin: 10px;">
		<script type='text/javascript'>
        GA_googleFillSlot("ArrobaBanner");
        </script>
    </div>

	<div id="columnists">
		<h2>Colunistas</h2>
		<div class="div_medium clear"></div>
        
		<? $col1 = get_highlight('Colunista 1') ?>
        <?php foreach ($col1 as $current): ?>
        <div class="post"> 
            <a href="<?= $current['permalink'] ?>"><?= get_avatar(get_the_author_meta('email'), 40) ?></a>
            <span class="iteration">
                <h3><? the_category(', '); ?> - <span class="author"><?php the_author(); ?></span></h3>
                <p><a href="<?= $current['permalink'] ?>"><?= $current['title'] ?></a></p>
            </span>
        </div>
        <!-- .post -->
        <div class="clear"></div>
        <? endforeach; ?> 
                        
		<? $col2 = get_highlight('Colunista 2') ?>
        <?php foreach ($col2 as $current): ?>
        <div class="post"> 
            <a href="<?= $current['permalink'] ?>"><?= get_avatar(get_the_author_meta('email'), 40) ?></a>
            <span class="iteration">
                <h3><? the_category(', '); ?> - <span class="author"><?php the_author(); ?></span></h3>
                <p><a href="<?= $current['permalink'] ?>"><?= $current['title'] ?></a></p>
            </span>
        </div>
        <!-- .post -->
        <div class="clear"></div>
        <? endforeach; ?> 
                        
		<? $col3 = get_highlight('Colunista 3') ?>
        <?php foreach ($col3 as $current): ?>
        <div class="post"> 
            <a href="<?= $current['permalink'] ?>"><?= get_avatar(get_the_author_meta('email'), 40) ?></a>
            <span class="iteration">
                <h3><? the_category(', '); ?> - <span class="author"><?php the_author(); ?></span></h3>
                <p><a href="<?= $current['permalink'] ?>"><?= $current['title'] ?></a></p>
            </span>
        </div>
        <!-- .post -->
        <div class="clear"></div>
        <? endforeach; ?> 
                        
		
		<p><a href="<?= get_category_link(get_cat_ID('colunas')) ?>" class="more">Mais colunistas »</a></p>
	</div>
	<!-- #columnists -->

  <p />
	
	<div id="social-media">
		<span class="twitter">
			<img src="<?= bloginfo('template_url') ?>/img/icon_twitter.jpg" />
			<p>Siga-me no Twitter <a href="http://twitter.com/rvianna" target="_blank">@rvianna</a></p>
		</span>
		<span class="facebook">
			<img src="<?= bloginfo('template_url') ?>/img/icon_facebook.jpg" />
			<p>Página no <a href="http://www.facebook.com/people/Rodrigo-Vianna/1065230505" target="_blank">Facebook</a></p>
		</span>
		<div class="clear"></div>
	</div>
	<!-- #social-media -->
	
	<div id="last-comments">
		<h2>Últimos comentários</h2>
		<div class="div_medium"></div>
		
		<? $comments = get_comments(array('number' => 3, 'status' => 'approve'));	?>
		<? foreach ($comments as $comment): ?>		
		<div class="comment">
			<?= get_avatar($comment->comment_author_email, 40) ?>
			<p class="excerpt"><a href="<?= get_permalink($comment->comment_post_ID) . '#comment-' . $comment->comment_ID ?>"><? comment_excerpt() ?></a></p>
			<p class="when"><?= time_ago('comment') ?></p>
		</div>
		<!-- .comment -->
		<? endforeach; ?>
	</div>
	<!-- #last-comments -->
    
    <? if(!is_home()): ?>
	<script src="http://www.redebrasilatual.com.br/scwidget.js?content=f488f3c382a37cd5c05c19f7fd2ab9b4" type="text/javascript"></script>
    <? endif; ?>

	<p />

	<div id="image">
		<h2>Imagem e Ação</h2>
		<div class="div_medium"></div>
		<br/>
        
		<? $foto = get_highlight('Foto da capa') ?>
        <?php foreach ($foto as $current): ?>
            <a href="<?= $post->image->uri; ?>" rel="lightbox[VEZ]" title="<?= the_excerpt(); ?>" ><img src="<?= $post->image->getThumbnailHref(array('w=300','q=250','fltr[]=usm|30|0.5|3')); ?>" alt="<?= the_title(); ?>" title="<?= the_title(); ?>" /></a>
            <p><?= the_excerpt(); ?></p>
            <p>&nbsp;</p>
        <? endforeach; ?>         
	
	</div>
	<!-- #image -->
    
	<div id="recommended-sites">
		<h2>Sites que indico</h2>
		<div class="div_medium"></div>		
		<br/>		
		<ul>
		<? get_links(19, '<li>', '</li>'); ?>
		</ul>
	</div>
	<!-- #recommended-sites -->
	<p />
	<div id="contact-me">
		<h2>Fale comigo</h2>
		<div class="div_medium"></div>
		<p><a href="mailto:escrevinhador.rv@hotmail.com">Escreva para mim enviando sugestões, matérias, indicações, críticas, denúncias que terei o prazer de ler e publicar.</a></p>
	</div>
	<!-- #contact-me -->

</div>
<!-- #sidebar -->