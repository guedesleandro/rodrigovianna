<div id="header">
	<div id="ad">
		<div class="inner">
			<div id="header-top">
				<script type='text/javascript'><!--//<![CDATA[
   var m3_u = (location.protocol=='https:'?'https://www.rodrigovianna.com.br/publicidade/www/delivery/ajs.php':'http://www.rodrigovianna.com.br/publicidade/www/delivery/ajs.php');
   var m3_r = Math.floor(Math.random()*99999999999);
   if (!document.MAX_used) document.MAX_used = ',';
   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
   document.write ("?zoneid=1");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write ("&amp;loc=" + escape(window.location));
   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
   if (document.context) document.write ("&context=" + escape(document.context));
   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
   document.write ("'><\/scr"+"ipt>");
//]]>--></script><noscript><a href='http://www.rodrigovianna.com.br/publicidade/www/delivery/ck.php?n=a97ed4df&amp;cb=123' target='_blank'><img src='http://www.rodrigovianna.com.br/publicidade/www/delivery/avw.php?zoneid=1&amp;cb=123&amp;n=a97ed4df' border='0' alt='' /></a></noscript>

			</div>
			<!-- #header-top -->
		</div>
		<!-- .inner -->
	</div>
	<!-- #ad -->

	<div id="main-header">
		<div class="inner">
			<a id="logo" href="<?php bloginfo('home'); ?>">
				<?php bloginfo('name'); ?>
			</a>					
			<!-- #logo -->

			<?= get_search_form() ?>

			<div id="week-thought">
				<h3 style="visibility: hidden;">Pensamento da semana</h3>
                
				<? $pensamento = get_highlight('Pensamento') ?>
                <?php foreach ($pensamento as $current): ?>
                <? the_excerpt(); ?>
                <? endforeach; ?> 
        
				<div>
					<span><img src="<?= bloginfo('template_url') ?>/img/mail.gif" /><a href="mailto:escrevinhador.rv@hotmail.com">fale comigo</a></span>
					<span><img src="<?= bloginfo('template_url') ?>/img/rss.gif" /><a href="<?php bloginfo('rss2_url'); ?>">rss</a></span>
				</div>
				<!-- #contact -->
			</div>
			<!-- #week-thought -->
		</div>
		<!-- .inner -->
	</div>
	<!-- main-header -->
	
	<?= get('menu') ?>

</div>
<!-- header -->