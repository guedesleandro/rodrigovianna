<head>
<meta name="google-site-verification" content="WoKtl8IjArMv5iLO6rlImTaTHc2wDQYHt-iJCBZSAb8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php
        global $page, $paged;
        wp_title( '-', true, 'right' );
        bloginfo( 'name' );
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo " - $site_description";
        if ( $paged >= 2 || $page >= 2 )
            echo ' | ' . sprintf( 'Página %s' ), max( $paged, $page );
        ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />		
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<?php wp_head(); ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
	<script language="javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-ui.js" type="text/javascript"></script>
	<script language="javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.tweet.js" type="text/javascript"></script>
	<script language="javascript" src="<?php bloginfo('template_directory'); ?>/js/default.js" type="text/javascript"></script>
    
</head>