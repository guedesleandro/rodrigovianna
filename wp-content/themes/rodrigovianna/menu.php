<div id="menu">
	<ul class="inner">
		<li class="plenos_poderes"><a href="<?= get_category_link(get_category_by_slug('plenos-poderes')) ?>">PLENOS PODERES</a></li>
		<li class="vasto_mundo"><a href="<?= get_category_link(get_category_by_slug('vasto-mundo')) ?>">VASTO MUNDO</a></li>
		<li class="forca_da_grana"><a href="<?= get_category_link(get_category_by_slug('forca-da-grana')) ?>">FORÇA DA GRANA</a></li>
		<li class="palavra_minha"><a href="<?= get_category_link(get_category_by_slug('palavra-minha')) ?>">PALAVRA MINHA</a></li>
		<li class="vestigios"><a href="<?= get_category_link(get_category_by_slug('vestigios')) ?>">VESTÍGIOS</a></li>
		<li class="outras_palavras"><a href="<?= get_category_link(get_category_by_slug('outras-palavras')) ?>">OUTRAS PALAVRAS</a></li>
		<li class="radar_da_midia"><a href="<?= get_category_link(get_category_by_slug('radar-da-midia')) ?>">RADAR DA MÍDIA</a></li>
		<li class="sopa_de_letras"><a href="<?= get_category_link(get_category_by_slug('sopa-de-letras')) ?>">SOPA DE LETRAS</a></li>
	</ul>
	<!-- .inner -->
</div>
<!-- menu -->