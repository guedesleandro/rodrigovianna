<?php
add_theme_support( 'post-thumbnails' );
function getIDBySlug($slug) {
	global $wpdb;
	$id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '$slug'");
	return $id;
}

function get($template) {
	include(TEMPLATEPATH . '/' . $template . '.php');
}

function have_categories($exclude = '') {
	global $cat;
	$result = false;
	$category = get_category($cat);
	$categoryID = $category->cat_ID;
	if($category->category_parent) {
		$categoryID = $category->category_parent;
	}
	if($exclude) $exclude = "&exclude=".$exclude;
	$result = get_categories('orderby=name'.$exclude.'&child_of='.$categoryID);
	return $result;
}

function searchFilter($query)
{
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}

add_filter('pre_get_posts','searchFilter');

function get_agenda() {
	global $wpdb;
	$results = $wpdb->get_results("SELECT * FROM $wpdb->posts");
	print_r($results);
	
	//$key_1_values = get_post_meta(76, 'key_1');
}


/**
 * Tests if any of a post's assigned categories are descendants of target categories
 *
 * @param int|array $cats The target categories. Integer ID or array of integer IDs
 * @param int|object $_post The post. Omit to test the current post in the Loop or main query
 * @return bool True if at least 1 of the post's categories is a descendant of any of the target categories
 * @see get_term_by() You can get a category by name or slug, then pass ID to this function
 * @uses get_term_children() Passes $cats
 * @uses in_category() Passes $_post (can be empty)
 * @version 2.7
 * @link http://codex.wordpress.org/Function_Reference/in_category#Testing_if_a_post_is_in_a_descendant_category
 */
function post_is_in_descendant_category( $cats, $_post = null )
{
	foreach ( (array) $cats as $cat ) {
		// get_term_children() accepts integer ID only
		$descendants = get_term_children( (int) $cat, 'category');
		if ( $descendants && in_category( $descendants, $_post ) )
			return true;
	}
	return false;
}

/**
 * New stuff
 */
function time_ago( $type = 'post' ) {
	$d = 'comment' == $type ? 'get_comment_time' : 'get_post_time';
	return human_time_diff($d('U'), current_time('timestamp')) . " " . __('atrás');
}

?>
