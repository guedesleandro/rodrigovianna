<div id="footer">
		<div id="sitemap" class="inner">
			<ul class="contact">
				<li class="about"><a href="sobre">sobre o blog</a></li>
				<li class="blogger"><a href="sobre-o-blogueiro">sobre blogueiro</a></li>                        
				<li class="mail"><a href="mailto:escrevinhador.rv@hotmail.com">fale comigo</a></li>
				<li class="rss"><a href="<?php bloginfo('rss2_url'); ?>">rss</a></li>
			</ul>
		</div>
		<!-- #sitemap -->

		<div class="clear"></div>

		<div id="notice" class="inner">
			<p class="legal">Reprodução de conteúdo autorizada com menção da fonte. As opiniões expressas no site são de responsabilidade dos autores</p>
			<p class="by"><a href="http://www.cafeazul.com.br/" title="Café Azul - Agência Digital"><img src="<?= bloginfo('template_url') ?>/img/by_cafe_azul.gif" /></a></p>
		</div>					
		<!-- #notice -->
</div>
<!-- #footer -->

<? wp_footer(); ?>