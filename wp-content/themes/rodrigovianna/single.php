<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
	<?= get('head') ?>
	
	<body>
    <?= get_header() ?>

		<div id="content">
			<div class="inner">
				<div id="main">
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>						
					<div id="article">
						<p class="tags"><?= the_tags('', ' » ', '') ?></p>
						<h1><?php the_title(); ?></h1>
						<div class="div_large"></div>
						<p class="date">publicada <?php the_time('l, d/m/Y') ?> às <? the_time('H:s') ?> e atualizada <? the_modified_time('l, d/m/Y') ?> às <? the_modified_time('H:s') ?></p>
            			<div class="text">
							<p>
								<? if(get_post_custom_values('youtube')): $youtubeArr = get_post_custom_values('youtube'); $youtube = $youtubeArr[0]; ?>
	            	<object width="580" height="340">
									<param name="movie" value="http://www.youtube.com/v/<?= $youtube; ?>&amp;rel=0">
									<param name="wmode" value="transparent">
									<embed src="http://www.youtube.com/v/<?= $youtube; ?>&amp;rel=0" type="application/x-shockwave-flash" wmode="transparent" width="580" height="340">
								</object>
	            	<? endif; ?>
							</p>	
							<p><?php the_content(); ?></p>
                            <? if(post_is_in_descendant_category(16)): ?>
                            	<?
								$category = get_the_category(); 
								$idCat = $category[0]->cat_ID;
								 ?>
                            	<p><a href="<?= get_category_link($idCat); ?>">Leia outros textos de <? the_author(); ?></a></p>
                            <? endif; ?>
                            <p>Leia outros textos de <?= the_category(', '); ?></p>
                            <?php if (function_exists('sociable_html')) { echo sociable_html(); } ?>
                            <?php if (function_exists('twitter_goodies_tweet_button')) { twitter_goodies_tweet_button(); } ?>
           				 </div>    
                        <!-- FullBanner -->
                        <div style="width: 468px; margin: 0 auto; text-align: center">
	                        <?php include (TEMPLATEPATH . '/banner-center.php'); ?>                                   
                        </div>
                          
						<p class="comments">
							<img src="<?= bloginfo('template_url') ?>/img/icon_comments.gif" /><? comments_number('Nenhum Comentário', '1 Comentário', '% Comentários' ) ?>
						</p>
						<?php comments_template(); ?>
					</div>
					<!-- article -->
					<?php endwhile; endif; ?>
                    
                                          
                    <div class="read">
						<h2>Veja mais</h2>
                        <ul>
	                        <?php query_posts('orderby=date&order=DESC&showposts=3&cat='.$cat); ?>
                        	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>			
                            <li><a href="<?= the_permalink(); ?>"><?= the_title(); ?></a></li>
                            <? endwhile; endif; ?>
                        </ul>
                    </div>
					
				</div>
				<!-- main -->
				
				<?= get_sidebar() ?>
				
				<div class="clear"></div>
			</div>
			<!-- .inner -->
		</div>
		<!-- #content -->

		<?= get_footer() ?>
	</body>
</html>