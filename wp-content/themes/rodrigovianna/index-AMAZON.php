<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
	<?= get('head') ?>
	
	<body>
    <?= get_header() ?>

		<div id="content">
			<div class="inner">
				<div id="main">			
					
					<? get_highlight('Manchete') ?>			
					<div id="article">
						<p class="tags"><?= the_tags('', ' » ', '') ?></p>
						<h1><a href="<? the_permalink() ?>"><? the_title() ?></a></h1>
						<div class="div_large"></div>
						<p class="date">publicada <?php the_time('l, d/m/Y') ?> às <? the_time('H:s') ?> e atualizada <? the_modified_time('l, d/m/Y') ?> às <? the_modified_time('H:s') ?></p>
            <div class="text">
							<p><? the_content('Leia a matéria completa »') ?></p>
            </div>                        
						<p class="comments">
							<img src="<?= bloginfo('template_url') ?>/img/icon_comments.gif" />
							<a href="<?= get_permalink() . '#comments' ?>"><? comments_number('Nenhum Comentário', '1 Comentário', '% Comentários' ) ?></a>
						</p>
					</div>
					<!-- article -->
					
					<div id="articles">
						<div id="highlights">
							<h2>Destaques</h2>
							<div class="div_small"></div>
							
							<? $args = array( 'thumb_w' => 82, 'thumb_h' => 82 ); $highlight1 = get_highlight('Destaque-1', $args) ?>
							<?php foreach ($highlight1 as $current): ?>
							<div class="highlight odd">
								<div class="title"><a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
									<span>
										<p class="tags"><?= the_tags('', ' » ', '') ?></p>
										<h3><a href="<? the_permalink() ?>"><? the_title() ?></a></h3>
									</span>
								</div>
								<p class="excerpt"><a href="<? the_permalink() ?>"><? the_excerpt() ?></a></p>
								<a href="<? the_permalink() ?>" class="read-more">Leia mais »</a>
							</div>
							<!-- .highlight -->
							<? endforeach; ?>
							
							<? $args = array( 'thumb_w' => 82, 'thumb_h' => 82 ); $highlight2 = get_highlight('Destaque-2', $args) ?>
							<?php foreach ($highlight2 as $current): ?>
							<div class="highlight even">
								<div class="title">
									<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
									<span>
										<p class="tags"><?= the_tags('', ' » ', '') ?></p>
										<h3><a href="<? the_permalink() ?>"><? the_title() ?></a></h3>
									</span>
								</div>
								<p class="excerpt"><a href="<? the_permalink() ?>"><? the_excerpt() ?></a></p>
								<a href="<? the_permalink() ?>" class="read-more">Leia mais »</a>
							</div>
							<!-- .highlight -->
							<? endforeach; ?>
						</div>
						<!-- #highlights -->

						<div id="latest-news">
							<h2>Mais recentes</h2>
							<div class="div_tiny"></div>

							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente1 = get_highlight('Recente 1', $args) ?>
							<?php foreach ($recente1 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente2 = get_highlight('Recente 2', $args) ?>
							<?php foreach ($recente2 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>   
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente3 = get_highlight('Recente 3', $args) ?>
							<?php foreach ($recente3 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>  
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente4 = get_highlight('Recente 4', $args) ?>
							<?php foreach ($recente4 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>  
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente5 = get_highlight('Recente 5', $args) ?>
							<?php foreach ($recente5 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente6 = get_highlight('Recente 6', $args) ?>
							<?php foreach ($recente6 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente7 = get_highlight('Recente 7', $args) ?>
							<?php foreach ($recente7 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?> 
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente8 = get_highlight('Recente 8', $args) ?>
							<?php foreach ($recente8 as $current): ?>
							<div class="article-excerpt">
								<? if ($current['image']): ?>
								<a href="<? the_permalink() ?>"><?= $current['image'] ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>                                                       
							
						</div>
						<!-- #latest-news -->
					</div>
					<!-- articles -->
				</div>
				<!-- main -->
				
				<?= get_sidebar() ?>
				
				<div class="clear"></div>
			</div>
			<!-- .inner -->
		</div>
		<!-- #content -->

		<?= get_footer() ?>
	</body>
</html>