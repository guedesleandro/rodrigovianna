<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
	<?= get('head') ?>
	
	<body>
    <?= get_header() ?>

		<div id="content">
			<div class="inner">
				<div id="main">			
					
					<? get_highlight('Manchete') ?>			
					<div id="article">
						<p class="tags"><?= the_tags('', ' » ', '') ?></p>
						<h1><a href="<? the_permalink() ?>"><? the_title() ?></a></h1>
						<div class="div_large"></div>
						<p class="date">publicada <?php the_time('l, d/m/Y') ?> às <? the_time('H:s') ?> e atualizada <? the_modified_time('l, d/m/Y') ?> às <? the_modified_time('H:s') ?></p>
            <div class="text">
							<p><? the_content('Leia a matéria completa »') ?></p>
            </div>                        
						<p class="comments">
							<img src="<?= bloginfo('template_url') ?>/img/icon_comments.gif" />
							<a href="<?= get_permalink() . '#comments' ?>"><? comments_number('Nenhum Comentário', '1 Comentário', '% Comentários' ) ?></a>
						</p>
					</div>
					<!-- article -->
					
                    
                    <div id="banner-center">
                    	<?php include (TEMPLATEPATH . '/banner-center.php'); ?>
                    </div><!--fecha banner-center-->
                    
					<div id="articles">
						<!--aqui ficava highlights-->
       <div id="videos">
		<? $tv = get_highlight('video') ?>
		<? foreach($tv as $current): ?>       
		<h2>Video</h2>
		<a href="<?= get_category_link(get_category_by_slug('videos')) ?>"><span class="more">Mais vídeos »</span></a>
	  <div class="div_medium clear"></div>
		<object width="319" height="260">
			<param name="movie" value="http://www.youtube.com/v/<?= $current['meta']['youtube']['0'] ?>&amp;rel=0">
			<param name="wmode" value="transparent">
			<embed src="http://www.youtube.com/v/<?= $current['meta']['youtube']['0'] ?>&amp;rel=0" type="application/x-shockwave-flash" wmode="transparent" width="336" height="203">
		</object>
        <h2><? the_title() ?></h2>
		<p><? the_excerpt() ?></p>
		<? endforeach; ?>  
	</div>
	<!-- #videos -->

						<div id="latest-news">
							<h2>Mais recentes</h2>
							<div class="div_tiny"></div>

							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente1 = get_highlight('Recente 1', $args) ?>
							<?php foreach ($recente1 as $current): ?>
							<div class="article-excerpt">
								<? if ( has_post_thumbnail() ): ?>
								<a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(45,45)); ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente2 = get_highlight('Recente 2', $args) ?>
							<?php foreach ($recente2 as $current): ?>
							<div class="article-excerpt">
								<? if ( has_post_thumbnail() ): ?>
								<a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(45,45)); ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>   
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente3 = get_highlight('Recente 3', $args) ?>
							<?php foreach ($recente3 as $current): ?>
							<div class="article-excerpt">
								<? if ( has_post_thumbnail() ): ?>
								<a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(45,45)); ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>  
                            
							<? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente4 = get_highlight('Recente 4', $args) ?>
							<?php foreach ($recente4 as $current): ?>
							<div class="article-excerpt">
								<? if ( has_post_thumbnail() ): ?>
								<a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(45,45)); ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>
                            
                            <? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente4 = get_highlight('Recente 5', $args) ?>
							<?php foreach ($recente4 as $current): ?>
							<div class="article-excerpt">
								<? if ( has_post_thumbnail() ): ?>
								<a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(45,45)); ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>   
                            
                            <? $args = array( 'thumb_w' => 45, 'thumb_h' => 45 ); $recente4 = get_highlight('Recente 6', $args) ?>
							<?php foreach ($recente4 as $current): ?>
							<div class="article-excerpt">
								<? if ( has_post_thumbnail() ): ?>
								<a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(45,45)); ?></a>
								<? endif; ?>
								<p><a href="<?= the_permalink() ?>"><?= the_title() ?></a></p>
								<p class="clear"></p>
							</div>
							<!-- .article-excerpt -->
              				<? endforeach; ?>                                               
							
						</div>
						<!-- #latest-news -->
                        
                        <div id="mais-comentados">
							<h2>Mais Comentados</h2>
							<div class="div_small"></div>
							<?php $popular = new WP_Query('orderby=comment_count&posts_per_page=5'); ?>

	<?php while ($popular->have_posts()) : $popular->the_post(); ?>	

	<?php $justanimage = get_post_meta($post->ID, 'Image', true);
			if ($justanimage) {
			?>
	<h3><img src="<?php echo get_post_meta($post->ID, "Image", true); ?>" alt="<?php the_title(); ?>" />

	<?php } else { ?>

	<h3>

	<?php } ?>

	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

<?php endwhile; ?>                                                    
							
						</div>
						<!-- #mais-comentados -->
                       
                            	<div id="image">
		<h2>Imagem e Ação</h2>
		<div class="div_tiny"></div>
		<br/>
        
		<? $foto = get_highlight('Foto da capa') ?>
        <?php foreach ($foto as $current): ?>
        
			<?php 
				if( has_post_thumbnail()) {
					$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
					echo '<a href="' . $large_image_url[0] . '" title="' . the_title_attribute('echo=0') . '" rel="lightbox[VEZ]">';
					the_post_thumbnail('medium');
					echo '</a>';
				 }
 			 ?>        
            <p><?= the_excerpt(); ?></p>
            <p>&nbsp;</p>
        <? endforeach; ?>         
	
	</div>
	<!-- #image -->
					</div>
					<!-- articles -->
				</div>
				<!-- main -->
				
				<?= get_sidebar() ?>
				
				<div class="clear"></div>
			</div>
			<!-- .inner -->
		</div>
		<!-- #content -->

		<?= get_footer() ?>
	</body>
</html>

