<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
	<?= get('head') ?>
	
	<body>
    <?= get_header() ?>

		<div id="content">
			<div class="inner">
				<div id="main">
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>						
					<div id="article">
						<p class="tags"><?= the_tags('', ' » ', '') ?></p>
						<h1><?php the_title(); ?></h1>
						<div class="div_large"></div>
						<p class="date">publicada <?php the_time('l, d/m/Y') ?> às <? the_time('H:s') ?> e atualizada <? the_modified_time('l, d/m/Y') ?> às <? the_modified_time('H:s') ?></p>
            <div class="text">
							<p><?php the_content(); ?></p>
            </div>                        
						<p class="comments">
							<img src="<?= bloginfo('template_url') ?>/img/icon_comments.gif" /><? comments_number('Nenhum Comentário', '1 Comentário', '% Comentários' ) ?>
						</p>
						<?php comments_template(); ?>
					</div>
					<!-- article -->
					<?php endwhile; endif; ?>
					
				</div>
				<!-- main -->
				
				<?= get_sidebar() ?>
				
				<div class="clear"></div>
			</div>
			<!-- .inner -->
		</div>
		<!-- #content -->

		<?= get_footer() ?>
	</body>
</html>