<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR">
	<?= get('head') ?>
	
	<body>
    <?= get_header() ?>

		<div id="content">
			<div class="inner">
				<div id="main">				
					
					<? if (have_posts()) : ?>
					<h2><? single_cat_title() ?></h2>
                        <div class="category_description"><?= category_description() ?></div>
					<hr noshade="noshade" />
					<ul id="search-results">
						<?php while (have_posts()) : the_post(); ?>
						<li>
							<h2><a href="<?= the_permalink(); ?>"><?= the_title() ?></a></h2>
              				<p><?= the_excerpt(); ?></p>              
						</li>
						<?php endwhile; ?>						
					</ul>
					<div class="paging"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>
					<? else: ?>
					<p>Não foi encontrada nenhuma matéria. Tente novamente!</p>
					<? endif; ?>
										
				</div>
				<!-- main -->
				
				<?= get_sidebar() ?>
				
				<div class="clear"></div>
			</div>
			<!-- .inner -->
		</div>
		<!-- #content -->

		<?= get_footer() ?>
	</body>
</html>