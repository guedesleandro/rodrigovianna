﻿<div id="sidebar">
	
	<div id="donation">
		<h2>Contribua com o site</h2>
		<div class="div_medium"></div>
		<p>Mantenha o Escrevinhador no ar. Clique no botão abaixo para fazer sua doação</p>
		<form target="pagseguro" action="https://pagseguro.uol.com.br/v2/pre-approvals/request.html" method="post">
		<input type="hidden" name="code" value="D4E0BF85AFAF82B00475EF8380EB5442" />
		<input type="image" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/assinaturas/120x53-assinar.gif" name="submit" alt="Contribua com o ESCREVINHADOR" />
		</form>
	</div>
	
    <div id="highlights">

        <h2>Destaques</h2>
        <div class="div_small"></div>
        
        <div id="dinamic">
        
			<? $highlight1 = get_highlight('Destaque-1') ?>
            <?php foreach ($highlight1 as $current): ?>
            <div id="h-<?= the_ID(); ?>" class="highlight odd">
                <div class="title"><a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(82,82)); ?></a>
                    <span>
                        <p class="tags"><?= the_tags('', ' » ', '') ?></p>
                        <h3><a href="<? the_permalink() ?>"><? the_title() ?></a></h3>
                    </span>
                </div>
                <p class="excerpt"><a href="<? the_permalink() ?>"><? the_excerpt() ?></a></p>
                <a href="<? the_permalink() ?>" class="read-more">Leia mais »</a>
            </div>
            <!-- .highlight -->
            <? endforeach; ?>
            
            <? $highlight1 = get_highlight('Destaque-2') ?>
            <?php foreach ($highlight1 as $current): ?>
            <div id="h-<?= the_ID(); ?>" class="highlight odd">
                <div class="title"><a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(82,82)); ?></a>
                    <span>
                        <p class="tags"><?= the_tags('', ' » ', '') ?></p>
                        <h3><a href="<? the_permalink() ?>"><? the_title() ?></a></h3>
                    </span>
                </div>
                <p class="excerpt"><a href="<? the_permalink() ?>"><? the_excerpt() ?></a></p>
                <a href="<? the_permalink() ?>" class="read-more">Leia mais »</a>
            </div>
            <!-- .highlight -->
            <? endforeach; ?> 
            
            <? $highlight1 = get_highlight('Destaque-3') ?>
            <?php foreach ($highlight1 as $current): ?>
            <div id="h-<?= the_ID(); ?>" class="highlight odd">
                <div class="title"><a href="<? the_permalink() ?>"><?= the_post_thumbnail(array(82,82)); ?></a>
                    <span>
                        <p class="tags"><?= the_tags('', ' » ', '') ?></p>
                        <h3><a href="<? the_permalink() ?>"><? the_title() ?></a></h3>
                    </span>
                </div>
                <p class="excerpt"><a href="<? the_permalink() ?>"><? the_excerpt() ?></a></p>
                <a href="<? the_permalink() ?>" class="read-more">Leia mais »</a>
            </div>
            <!-- .highlight -->
            <? endforeach; ?>               
            
            <ul>
                <? $highlight1 = get_highlight('Destaque-1') ?>
                <?php foreach ($highlight1 as $current): ?>        
                <li><a href="#h-<?= the_ID(); ?>"><?= the_title(); ?></a></li>
                <? endforeach; ?> 
                <? $highlight1 = get_highlight('Destaque-2') ?>
                <?php foreach ($highlight1 as $current): ?>        
                <li><a href="#h-<?= the_ID(); ?>"><?= the_title(); ?></a></li>
                <? endforeach; ?> 
                <? $highlight1 = get_highlight('Destaque-3') ?>
                <?php foreach ($highlight1 as $current): ?>        
                <li><a href="#h-<?= the_ID(); ?>"><?= the_title(); ?></a></li>
                <? endforeach; ?> 
            </ul>
            
		</div>
        <!-- dinamic -->           
                            
	</div>
	<!-- #highlights -->
    <SCRIPT type="text/javascript">
	var ADM_iq = '//ia.nspmotion.com';
	try { ADM_iq = (document.location.protocol=='https:'?'https:':'http:')+ADM_iq } catch(e){}
	document.write('<scr'+'ipt type="text/javascr'+'ipt" src="'+ADM_iq+'/delivery/?p=163111&sc=7802&r='+Math.round(Math.random()*99999)+'"></scr'+'ipt>');
	</SCRIPT>

	<NOSCRIPT>
	<iframe src="//ia.nspmotion.com/delivery-noscript/?p=163111&sc=7802" width="300" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" allowtransparency="true"></iframe>
	</NOSCRIPT>
	<!--script type="text/javascript"><!--
	google_ad_client = "ca-pub-6288739839094983";
	/* Sidebar */
	google_ad_slot = "1347182953";
	google_ad_width = 300;
	google_ad_height = 250;
	//-->
	<!--/script>
	<script type="text/javascript"
	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script-->

	

  <p />
	
	<!--<div id="social-media">
		<span class="twitter">
			<img src="<?= bloginfo('template_url') ?>/img/icon_twitter.jpg" />
			<p>Siga-me no Twitter <a href="http://twitter.com/rvianna" target="_blank">@rvianna</a></p>
		</span>
		<span class="facebook">
			<img src="<?= bloginfo('template_url') ?>/img/icon_facebook.jpg" />
			<p>Página no <a href="http://www.facebook.com/people/Rodrigo-Vianna/1065230505" target="_blank">Facebook</a></p>
		</span>
		<div class="clear"></div>
	</div>
	<!-- #social-media -->
    
    <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FEscrevinhador%2F360573263963286&amp;width=336&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false&amp;appId=166441460130918" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:336px; height:258px;" allowTransparency="true"></iframe>
    
    
    <div id="twitter">
		<h2>Twitter</h2>
		<div class="div_medium"></div>
		<!--div id="twitter"></div-->
		<?= do_shortcode('[aktt_tweets account="rvianna" count="5" offset="0"]'); ?>
        
	</div><span class="twitter-siga-me">Siga-me no Twitter <a href="http://twitter.com/#!/rvianna">@rvianna</a></span>
	<!-- #twitter -->
    
	
	<div id="last-comments">
		<h2>Últimos comentários</h2>
		<div class="div_medium"></div>
		
		<? $comments = get_comments(array('number' => 2, 'status' => 'approve'));	?>
		<? foreach ($comments as $comment): ?>		
		<div class="comment">
			<?= get_avatar($comment->comment_author_email, 40) ?>
			<p class="excerpt"><a href="<?= get_permalink($comment->comment_post_ID) . '#comment-' . $comment->comment_ID ?>"><? comment_excerpt() ?></a></p>
			<p class="when"><?= time_ago('comment') ?></p>
		</div>
		<!-- .comment -->
		<? endforeach; ?>
	</div>
	<!-- #last-comments -->
    
    
    
    
    <!--div id="columnists">
		<h2>Colunistas</h2>
		<div class="div_medium clear"></div>
        
		<? $col1 = get_highlight('Colunista 1') ?>
        <?php foreach ($col1 as $current): ?>
        <div class="post"> 
            <a href="<?= $current['permalink'] ?>"><?= get_avatar(get_the_author_meta('email'), 40) ?></a>
            <span class="iteration">
                <h3><? the_category(', '); ?> - <span class="author"><?php the_author(); ?></span></h3>
                <p><a href="<?= $current['permalink'] ?>"><?= $current['title'] ?></a></p>
            </span>
        </div>
        
        <div class="clear"></div>
        <? endforeach; ?> 
                        
		<? $col2 = get_highlight('Colunista 2') ?>
        <?php foreach ($col2 as $current): ?>
        <div class="post"> 
            <a href="<?= $current['permalink'] ?>"><?= get_avatar(get_the_author_meta('email'), 40) ?></a>
            <span class="iteration">
                <h3><? the_category(', '); ?> - <span class="author"><?php the_author(); ?></span></h3>
                <p><a href="<?= $current['permalink'] ?>"><?= $current['title'] ?></a></p>
            </span>
        </div>

        <div class="clear"></div>
        <? endforeach; ?> 
                        
		<? $col3 = get_highlight('Colunista 3') ?>
        <?php foreach ($col3 as $current): ?>
        <div class="post"> 
            <a href="<?= $current['permalink'] ?>"><?= get_avatar(get_the_author_meta('email'), 40) ?></a>
            <span class="iteration">
                <h3><? the_category(', '); ?> - <span class="author"><?php the_author(); ?></span></h3>
                <p><a href="<?= $current['permalink'] ?>"><?= $current['title'] ?></a></p>
            </span>
        </div>

        <div class="clear"></div>
        <? endforeach; ?> 
                        
		
		<p><a href="<?= get_category_link(get_cat_ID('colunas')) ?>" class="more">Mais colunistas »</a></p>
	</div>
	<!-- #columnists -->
    
    
    <? if(!is_home()): ?>
	<script src="http://www.redebrasilatual.com.br/scwidget.js?content=f488f3c382a37cd5c05c19f7fd2ab9b4" type="text/javascript"></script>
    <? endif; ?>

	<p />

<!--aqui ficava image-->
    
	<div id="recommended-sites">
		<h2>Sites que indico</h2>
		<div class="div_medium"></div>		
		<br/>		
		<ul>
		<? get_links(19, '<li>', '</li>'); ?>
		</ul>
	</div>
	<!-- #recommended-sites -->
	<p />
	<!--<div id="contact-me">
		<h2>Fale comigo</h2>
		<div class="div_medium"></div>
		<p><a href="mailto:escrevinhador.rv@hotmail.com">Escreva para mim enviando sugestões, matérias, indicações, críticas, denúncias que terei o prazer de ler e publicar.</a></p>
	</div>
	<!-- #contact-me -->

</div>
<!-- #sidebar -->