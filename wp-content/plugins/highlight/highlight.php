<?php
/*
Plugin Name: Highlight
Description: Permite colocar um post em destaque em um determinado lugar do blog/site.
Version: 0.1
Author: Leandro Guedes
*/

function get_highlight($name, $opts = array(
											'thumb_w' => 80,
											'thumb_h' => 80 )
											) {
    global $post;
	global $wpdb, $table_prefix;
	$table_name = $table_prefix.'highlights';
	
	$sql = "SELECT post_id FROM $table_name WHERE name = '".$name."'";
	$postId = $wpdb->get_var($wpdb->prepare($sql));
	$highlighted_arr = explode(',',$postId);

/*
	query_posts(array(
    	'post__in' => $highlighted_arr,
	));
	return $highlighted_arr;
	*/
	$out = array();
	for ($i = 0; $i < count($highlighted_arr); $i++){
		$post = new WP_Query('p=' . $highlighted_arr[$i]);
		$post->the_post();
		$the_image = (function_exists('yapb_is_photoblog_post') && yapb_is_photoblog_post()) ? yapb_get_thumbnail(
			'', // HTML before image tag
			array(
				'alt' => stripslashes($post->post_title), // image tag alt attribute
			),
			'',               // HTML after image tag
			array('w='.$opts['thumb_w'], 'h='.$opts['thumb_h'], 'q=90'), // phpThumb configuration parameters
			'thumbnail'             // image tag custom css class
		) : 0;
		$categories = get_the_category($post->ID);
		
		$attachs = get_children(
			array(
			'post_parent' => $post->ID,
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			/*'post_mime_type' => 'image',*/
			'order' => 'ASC',
			'orderby' => 'menu_order'
				)
			);
			$attachment = array();		
			if($attachs) {
				foreach ( $attachs as $id => $attach ) {
					array_push($attachment, $attach);
				}
			}

		
		$the_post = array(
			'id'      => $post->ID,
			'title'   => stripslashes($post->post_title),
			'excerpt' => stripslashes(get_the_excerpt($highlighted_arr[$i])),
			'permalink' => get_permalink($highlighted_arr[$i]),
			'image' => $the_image,
			'categories' => $categories[0]->name,
			'author' => $post->author,
			'attachment' => $attachment,
			'meta' => get_post_meta($post->ID,''),
		);
		array_push( $out, $the_post);
	}
	return $out;
}

//Installer
function install_highlight() {
	
	global $wpdb, $table_prefix;
	$table_name = $table_prefix.'highlights';
	
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
		$sql = "CREATE TABLE $table_name (
					id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
					post_id bigint(20) UNSIGNED NOT NULL,
					name varchar(255) NOT NULL,
					created datetime NOT NULL,
					modified datetime NOT NULL,
					UNIQUE KEY id(id)
				);";
		$rs = $wpdb->query($sql);
		
		$currentDate = date('Y-m-d H:i:s');
		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'Manchete', '$currentDate', '$currentDate')";
		$wpdb->query($sql);
		/*
		
		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'Destaque 1', '$currentDate', '$currentDate')";
		$wpdb->query($sql);		
		
		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'Destaque 2', '$currentDate', '$currentDate')";
		$wpdb->query($sql);	
		
		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'Destaque 3', '$currentDate', '$currentDate')";
		$wpdb->query($sql);		


		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'Destaque 4', '$currentDate', '$currentDate')";
		$wpdb->query($sql);		

		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'Radio', '$currentDate', '$currentDate')";
		$wpdb->query($sql);		
		
		$sql = "INSERT INTO $table_name (post_id, name, created, modified) VALUES (0, 'TV', '$currentDate', '$currentDate')";
		$wpdb->query($sql);		
		*/		
	}
}

function add_posts_column_highlight($defaults) {
    global $current_user;
    get_currentuserinfo();   
    
    //if ($current_user->allcaps['level_' . $yafpp_opts['admin_level']]) {
        $defaults['Destacar'] = 'Destacar';
    //}
    
    return $defaults;
}

function posts_column_highlight($column_name, $postId) {
	global $wpdb, $table_prefix;
	$table_name = $table_prefix.'highlights';
	
	$root = get_option('home');
    $highlight_dir = $root . '/wp-content/plugins/highlight';	
	
	if ( $column_name == 'Destacar' ) {
		echo "<select id='highlight_$postId' class='highlight_select'>";
		echo "<option value=\"0\" selected=\"selected\">Selecione</option>";
		$sql = "SELECT name, id FROM $table_name";
		$results = $wpdb->get_results($sql);
		foreach($results as $key => $values) {
			echo "<option value=\"".$values->id . "#". $postId ."\">".$values->name."</option>";
		}
		echo "</select>";
        echo '<img src="' . $highlight_dir . '/img/loading.gif" alt="Carregando" class="highlight_loading" id="highlight_loading_'.$postId.'" />';
    }
}

function highlight_ajax_feature_js() {
	wp_print_scripts( array( 'sack', 'jquery' ));
    ?>
    <script type="text/javascript">
    //<![CDATA[
			   
	jQuery(document).ready(function() {
		jQuery(".highlight_select").change(function() 
		{ 
			value = jQuery(this).val();
			array = value.split("#");
			id = array[0];
			postId = array[1];
			highlighter(postId, id);
		}); 
	 });	
			   
    function highlighter(postId, highlightId) {
        jQuery('#highlight_loading_' + postId).toggle();
		var mysack = new sack("<?php bloginfo( 'wpurl' ); ?>/wp-admin/admin-ajax.php");

        mysack.execute = 1;
        mysack.method = 'POST';
        mysack.setVar("action", "highlight_process");
        mysack.setVar("post_id", postId);
        mysack.setVar("highlight_id", highlightId);		
        mysack.encVar("cookie", document.cookie, false);
        mysack.onError = function() { alert('Erro! Tente novamente.' )};
        mysack.runAJAX();
    
    return true;
    }
    //]]>
    </script>
<?php
}

function highlight_css() {
    $out = <<<EOT
<!-- Highlight -->
<style type="text/css">
.highlight_loading {
    display: none;
}
</style>
<!-- end Highlight -->
EOT;
    echo $out;
}


function highlight_process_feature() {
	global $wpdb, $table_prefix;
	$table_name = $table_prefix.'highlights';
	
    $highlight_id = $_POST['highlight_id'];
	$postId = $_POST['post_id'];
	$sql = "UPDATE $table_name SET post_id = $postId WHERE id = $highlight_id";
	$wpdb->query($sql);
	
	$js = "jQuery('#highlight_loading_".$postId."').toggle();";
    die( $js );	
}

add_action('wp_ajax_highlight_process', 'highlight_process_feature' );
add_action('admin_print_scripts', 'highlight_ajax_feature_js' );
add_action('admin_head', 'highlight_css');
add_filter('manage_posts_columns', 'add_posts_column_highlight');
add_filter('manage_posts_custom_column', 'posts_column_highlight', 10, 2);
add_action('activate_highlight', 'install_highlight');

?>