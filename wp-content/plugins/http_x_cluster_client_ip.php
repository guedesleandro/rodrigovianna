<?php
/*
Plugin Name: HTTP_X_CLUSTER_CLIENT_IP Support
Plugin URI: http://www.bluetrait.com/page/wordpress-plugins/
Description: This plugin modifies the users ip address so that it has the correct value if running behind a cluster.
Version: 0.1
Author: Michael Dale
Author URI: http://www.bluetrait.com/
*/

add_action('init', 'http_x_cluster_client_ip');

function http_x_cluster_client_ip()  {
	global $_SERVER;
	
	//if we're running behind a cluster change the IP address.
	if (array_key_exists('HTTP_CF_CONNECTING_IP', $_SERVER)) {
			$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
	}
	
	return;
	
}

?>