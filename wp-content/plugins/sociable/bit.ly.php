<?php
	function get_bit_ly_url($url,$bitlylogin,$bitlyapikey)
	{		
		$shortUrl = $url;
		$requestUrl = 'http://api.bit.ly/shorten?version=2.0.1&longUrl='.$url.'&login='.$bitlylogin.'&apiKey='.$bitlyapikey;
		$result = json_decode(file_get_contents($requestUrl),true);
		if($result!=null && isset($result['results'][urldecode($url)]['shortUrl']))
				$shortUrl = $result['results'][urldecode($url)]['shortUrl'];
				
		return $shortUrl;
	}
	
	define('DOING_AJAX',true);

	require('../../../wp-load.php');
	
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

	$bitlylogin 	= get_option('sociable_bitlylogin');
	$bitlyapikey	= get_option('sociable_bitlyapikey');
	$url = get_option('home');
	
	if(isset($_GET['t']) && !empty($bitlylogin) && !empty($bitlyapikey))
	{
		if(isset($_GET['u']))
		{
			$shortUrl = get_bit_ly_url(urlencode($_GET['t']),$bitlylogin,$bitlyapikey);
			$url = str_replace('PERMALINK', $shortUrl, $_GET['u']);
		}
		else
			$url = $_GET['t'];
	}
			
	$url = str_replace("+","%20",$url);
	header("Location: ".$url);
?>